﻿using System.Web;
using System.Web.Mvc;

namespace APC_jaime_moreno
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
