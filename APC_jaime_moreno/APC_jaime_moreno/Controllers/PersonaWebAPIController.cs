﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using APC_jaime_moreno;
using Entidades;
using DALL;
using BLL;
using Entidades.ViewModels;

namespace APC_jaime_moreno.Controllers
{

    public class PersonaWebAPIController : ApiController
    {
        private jaime_morenoEntities db = new jaime_morenoEntities();
        protected PersonaBLL objectBLL = new PersonaBLL();

        // GET: api/PersonaWebAPI
        public ResponseViewModel GetPersona() => objectBLL.ObtenerPersonas();

        // GET: api/PersonaWebAPI/5
        public ResponseViewModel GetPersona(int id)
        {
            return objectBLL.ObtenerPersona(id);
        }

        // GET: api/PersonaWebAPI
        public ResponseViewModel GetPersona(int TipoDocumentoId, string Documento)
        {
            return objectBLL.ObtenerPersonaPorDocumento(TipoDocumentoId, Documento);
        }

        // PUT: api/PersonaWebAPI/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPersona(int id, Persona persona)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != persona.PersonaId)
            {
                return BadRequest();
            }

            db.Entry(persona).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PersonaWebAPI
        //[ResponseType(typeof(Persona))]
        public async Task<IHttpActionResult> PostPersona(Persona persona) //, string Apellidos, 
            //int TipoDocumentoId, string Documento, int GeneroId, int Edad)
        {


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            await objectBLL.RegistarPersona(persona);

            //db.Persona.Add(persona);
            //await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = persona.PersonaId }, persona);
        }

        // DELETE: api/PersonaWebAPI/5
        [ResponseType(typeof(Persona))]
        public async Task<IHttpActionResult> DeletePersona(int id)
        {
            Persona persona = await db.Persona.FindAsync(id);
            if (persona == null)
            {
                return NotFound();
            }

            db.Persona.Remove(persona);
            await db.SaveChangesAsync();

            return Ok(persona);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PersonaExists(int id)
        {
            return db.Persona.Count(e => e.PersonaId == id) > 0;
        }
    }
}