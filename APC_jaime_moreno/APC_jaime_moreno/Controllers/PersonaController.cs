﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using APC_jaime_moreno;
using DALL;
using BLL;
using System.Threading.Tasks;

namespace APC_jaime_moreno.Controllers
{
    public class PersonaController : Controller
    {
        private jaime_morenoEntities db = new jaime_morenoEntities();
        protected  PersonaBLL objectBLL = new PersonaBLL();

        // GET: Personas
        public ActionResult Index()
        {
            var response = objectBLL.ObtenerPersonas();
            return View(response.Resultado);
        }

        // GET: Personas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            ViewBag.GeneroId = new SelectList(db.Genero, "GeneroId", "Descripcion");
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumento, "TipoDocumentoId", "Descripcion");
            return View();
        }

        // POST: Personas/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PersonaId,Nombres,Apellidos,TipoDocumentoId,Documento,GeneroId,Edad,Estado,FechaHoraRegistro,FechaHoraModificacion")] Persona persona)
        {
            if (ModelState.IsValid)
            {
                await objectBLL.RegistarPersona(persona);
                return RedirectToAction("Index");
            }

            ViewBag.GeneroId = new SelectList(db.Genero, "GeneroId", "Descripcion", persona.GeneroId);
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumento, "TipoDocumentoId", "Descripcion", persona.TipoDocumentoId);
            return View(persona);
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.GeneroId = new SelectList(db.Genero, "GeneroId", "Descripcion", persona.GeneroId);
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumento, "TipoDocumentoId", "Descripcion", persona.TipoDocumentoId);
            return View(persona);
        }

        // POST: Personas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PersonaId,Nombres,Apellidos,TipoDocumentoId,Documento,GeneroId,Edad,Estado,FechaHoraRegistro,FechaHoraModificacion")] Persona persona)
        {
            persona.FechaHoraModificacion = DateTime.Now;
            persona.Estado = persona.Estado != null ? persona.Estado : false;

            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GeneroId = new SelectList(db.Genero, "GeneroId", "Descripcion", persona.GeneroId);
            ViewBag.TipoDocumentoId = new SelectList(db.TipoDocumento, "TipoDocumentoId", "Descripcion", persona.TipoDocumentoId);
            return View(persona);
        }

        // GET: Personas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Persona.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Persona persona = (Persona)db.Persona.Where(x => x.PersonaId == id).First();
            await objectBLL.EliminarPersona(persona.TipoDocumentoId, persona.Documento);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
