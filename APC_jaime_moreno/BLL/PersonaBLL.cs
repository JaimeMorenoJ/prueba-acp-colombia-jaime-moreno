﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using APC_jaime_moreno;
using DALL;
using Entidades;
using Entidades.ViewModels;

/* BLL (Business Logical Layer – Capa de Lógica de Negocio)
 *
 * Validaciones
     * 1. Un usuario no se puede registrar dos veces (la llave única para verificar que un usuario
     * ya existe es el tipo de documento y el documento).
     * 
     * 2. La eliminación de un usuario debe ser a nivel lógico (tenga en cuenta la columna Estado).
     * 
     * 3. Solo se deben traer los registros en Estado activos de personas, géneros y tipos de 
     * documentos (tenga en cuenta la columna Estado).
     * 
     * 4. Los mensajes que retornan los servicios deben ser entendibles.
*/
namespace BLL
{

    public class PersonaBLL
    {
        public jaime_morenoEntities dbcontext;
        protected PersonaDALL objectDLL;

        public PersonaBLL()
        {
            dbcontext = new jaime_morenoEntities();
            objectDLL = new PersonaDALL();
        }

        public async Task<ResponseViewModel> RegistarPersona(Persona persona)
        {
            try
            {
                persona.FechaHoraModificacion = DateTime.Now; // Hora actual, se toma como hora de resgistro 
                persona.FechaHoraRegistro = DateTime.Now;     // Hora actual, se toma como hora de actualizacion 
                persona.Estado = true;

                //Se validar que al registrar una nueva persona esta no exista en el sistema

                bool PersonaExiste = dbcontext.Persona.Any
                 (x => x.TipoDocumentoId == persona.TipoDocumentoId && x.Documento == persona.Documento);

                if (PersonaExiste == true)
                {
                    return objectDLL.Response(false, "Error al registrar la persona ingresada, la persona ya exite en el sistema", null);
                }
                else
                {
                    return await objectDLL.RegistarPersona(persona, dbcontext); // Invocacion metodo registrar persona DALL
                }               
            }
            catch
            {
                return objectDLL.Response(false, "Error al registrar una nueva persona", null);
            }
        }

        public async Task<ResponseViewModel> EliminarPersona(int TipoDocumentoId, string Documento)
        {
            try
            {
                bool PersonaExiste = dbcontext.Persona.Any
                    (x => x.TipoDocumentoId == TipoDocumentoId && x.Documento == Documento);
                if (PersonaExiste == true)
                {
                    // Eliminacion logica o cambio de estado del registro se realiza en la capa DALL con usando el contexto, dado que los parametros
                    // Del metodo solicitado unicamentesolicitan el tipo de documento y documento 
                    return await objectDLL.EliminarPersona(TipoDocumentoId, Documento, dbcontext); // Invocacion metodo eliminar persona DALL
                }
                else
                {
                    return objectDLL.Response(false, "Error al eliminar la persona", null);
                }
            }
            catch
            {
                return objectDLL.Response(false, "Error al intentar eliminar la persona", null);
            }            
        }

        public ResponseViewModel ObtenerPersonas()
        {
            try
            {
                return objectDLL.ObtenerPersonas(dbcontext); // Solo se trae el registro de las personas activas en el sistema
            }
            catch
            {
                return objectDLL.Response(false, "Error al intentar obtener el listado personas registradas", null);
            }
        }

        public ResponseViewModel ObtenerPersona(int PersonaId)
        {
            try
            {
                return objectDLL.ObtenerPersona(PersonaId, dbcontext); // Solo se trae el registro de las personas activas en el sistema
            }
            catch
            {
                return objectDLL.Response(false, "Error al intentar obtener la persona con el ID de registro" + PersonaId.ToString(), null);
            }
        }

        //Obtener Persona con documento 
        //Se considera el Documento como un dato de tipo string dado que los documentos de cedulas de ciudadania extranjera por lo general cuentan con caracteres especiales
        public ResponseViewModel ObtenerPersonaPorDocumento(int TipoDocumentoId, string Documento)
        {
            try
            {
                return objectDLL.ObtenerPersonaPorDocumento(TipoDocumentoId, Documento, dbcontext); // Solo se trae el registro de personas activas en el sistema
            }
            catch
            {
                return objectDLL.Response(false, "Error al intentar obtener la persona con el tipo de documeno de identidad " + TipoDocumentoId.ToString() + " y documento de identificación " + Documento, null);
            }            
        }

    }
}
 