﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using APC_jaime_moreno;
using Entidades;
using Entidades.ViewModels;


namespace DALL{
    public class PersonaDALL
    {
        public async Task<ResponseViewModel> RegistarPersona(Persona persona, jaime_morenoEntities dbcontext)
        {
            try
            {
                //Metodo para registar personas
                dbcontext.Persona.Add(persona);
                await dbcontext.SaveChangesAsync();

                return new ResponseViewModel()
                {
                    Estado = true,
                    Mensaje = "Se registro la persona exitosamente!",
                    Resultado = (object)("OK")
                };
            }
            catch
            {
                return Response(false, "No se logro registrar la persona",  null);
            }
        }

        public ResponseViewModel ObtenerPersonas(jaime_morenoEntities dbcontext)
        {
            try
            {
                //Consulta personas no eliminadas
                var personas = dbcontext.Persona.Where(x => x.Estado == true)
                    .Include(p => p.Genero).Include(p => p.TipoDocumento); 

                return new ResponseViewModel()
                {
                    Estado = true,
                    Mensaje = "Consulta de personas exitosa!",
                    Resultado = (object)personas.ToList()
                };
            }
            catch
            {
                return Response(false, "No se lograron obtener las personas registradas", null);
            }
        }

        public ResponseViewModel ObtenerPersona(int PersonaId, jaime_morenoEntities dbcontext)
        {
            try
            {
                //Consulta personas no eliminadas
                var persona = dbcontext.Persona.Where(x => x.Estado == true && x.PersonaId == PersonaId).FirstOrDefault(); 
                return new ResponseViewModel()
                {
                    Estado = true,
                    Mensaje = "Se obtuvo un registro para la consulta de persona con PersonID: "+ PersonaId,
                    Resultado = (object)persona
                };
            }
            catch
            {
                return Response(false, "No se encontraron resultados para las personas consultadas", null);
            }
        }

        public ResponseViewModel ObtenerPersonaPorDocumento(int TipoDocumentoId, string Documento, jaime_morenoEntities dbcontext)
        {
            try
            {
                // Metodo para obtener personas por documento
                var persona = (Persona) dbcontext.Persona.Where
                        (x => x.TipoDocumentoId == TipoDocumentoId & x.Documento == Documento).SingleOrDefault();

                return new ResponseViewModel()
                {
                    Estado = true,
                    Mensaje = "Se logro obtener registros con el tipo de documento y el documento consultado, exitosamente!",
                    Resultado = (object)persona
                };
            }
            catch
            {
                return Response(false, "No se logro obtener las personas consultadas", null);
            }
        }
         
        public async Task<ResponseViewModel> EliminarPersona(int TipoDocumentoId, string Documento, jaime_morenoEntities dbcontext)
        {
            try
            {
                // Metodo para obtener personas por documento
                var persona = (Persona)this.ObtenerPersonaPorDocumento(TipoDocumentoId, Documento, dbcontext).Resultado;
                //var tipoDocumento = (TipoDocumento)dbcontext.TipoDocumento.Where(x => x.TipoDocumentoId == TipoDocumentoId);

                persona.FechaHoraModificacion = DateTime.Now;
                persona.Estado = false;

                dbcontext.Entry(persona).State = EntityState.Modified;
                // Se comentarea la posibilidad de eliminar los registros fisicamente 
                //dbcontext.Persona.Remove(persona); 
                await dbcontext.SaveChangesAsync();

                return new ResponseViewModel()
                {
                    Estado = true,
                    Mensaje = "Se elimino la persona con el tipo de documento y el documento consultado, exitosamente!",
                    Resultado = (object)("OK")
                };
            }
            catch
            {
                return Response(false, "No se logro eliminar la persona", null);
            }
        }

        // Se genera una funcion generica que facilite el manejo de errores de la capa de negocio.
        public ResponseViewModel Response(bool Estado, string Mensaje, object Resultado)
        {
            return new ResponseViewModel()
            {
                Estado = Estado,
                Mensaje = Mensaje,
                Resultado = Resultado
            };
        }

    }
}