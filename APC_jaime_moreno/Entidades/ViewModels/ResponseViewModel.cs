﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entidades.ViewModels
{ 
    public class ResponseViewModel
    {
        public bool Estado { set; get; }
        public string Mensaje { set; get; }
        public object Resultado { set; get; }
    }
}